package com.thang.employeemanagement

import android.os.Bundle
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.thang.employeemanagement.networking.volleys.VolleyNetworkRequest
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //init volley, pass the context to it
        VolleyNetworkRequest.getInstance(this)

        val navController = findNavController(R.id.nav_host_fragment)
        bottom_navigation.setupWithNavController(navController)
        navController.addOnDestinationChangedListener {
            _, destination, _ ->
            if (destination.id == R.id.loginFragment) {
                supportActionBar?.hide()
                bottom_navigation.visibility = INVISIBLE
            } else {
                supportActionBar?.show()
                bottom_navigation.visibility = VISIBLE
            }
        }
    }
}
