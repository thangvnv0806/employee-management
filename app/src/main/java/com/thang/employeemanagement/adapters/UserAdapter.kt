package com.thang.employeemanagement.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thang.employeemanagement.R
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.android.synthetic.main.user_item.view.*

class UserAdapter(
    val users: MutableList<User>,
    val roleId: String,
    val onCommandClick: (Int, User) -> Unit
) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        const val VIEW_COMMAND = 0
        const val UPDATE_COMMAND = 1
        const val DELETE_COMMAND = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_item, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = users[position]
        val view = holder.itemView

        view.user_item_name_text.text = user.fullName
        view.user_item_role_text.text = user.roleName

        if (roleId == "admin") {
            view.setOnClickListener { onCommandClick(UPDATE_COMMAND, user) }
        } else {
            // by default is view only
            view.setOnClickListener { onCommandClick(VIEW_COMMAND, user) }
        }
        view.user_delete_btn.setOnClickListener { onCommandClick(DELETE_COMMAND, user) }
    }
}