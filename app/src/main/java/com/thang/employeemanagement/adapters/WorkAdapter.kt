package com.thang.employeemanagement.adapters

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.thang.employeemanagement.R
import com.thang.employeemanagement.fragments.UserFinishDialogFragment
import com.thang.employeemanagement.helpers.fromDateToString
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.presenters.WorkItemPresenter
import kotlinx.android.synthetic.main.manager_finish_confirm_dialog.view.*
import kotlinx.android.synthetic.main.work_load_item.view.*
import java.util.*

class WorkAdapter(
    val works: MutableList<Work>,
    val user: User,
    val onCommandClick: (Int, Work) -> Unit
) :
    RecyclerView.Adapter<WorkAdapter.WorkViewHolder>() {

    class WorkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        const val VIEW_COMMAND = 0
        const val UPDATE_COMMAND = 1
        const val DELETE_COMMAND = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.work_load_item, parent, false)
        return WorkViewHolder(view)
    }

    override fun getItemCount(): Int = works.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: WorkViewHolder, position: Int) {
        val itemView = holder.itemView
        val work = works[position]
        val popupMenu = PopupMenu(itemView.context, itemView.work_menu_btn)
        val workItemView = object : WorkItemPresenter.View {

            override fun onCommandClick(command: Int, work: Work) {
                itemView.setOnClickListener { this@WorkAdapter.onCommandClick(command, work) }
            }

            override fun add(menuId: Int, label: String) {
                popupMenu.menu.add(Menu.NONE, menuId, Menu.NONE, label)
            }

            override fun onWorkDeleted(position: Int) {
                this@WorkAdapter.notifyItemRemoved(position)
            }

            override fun onWorkUpdate(position: Int) {
                this@WorkAdapter.notifyItemChanged(position)
            }
        }
        val presenter = WorkItemPresenter(workItemView)

        itemView.work_item_id_name_creator_handler_text.text =
            "${work.workId} - ${work.workName}. ${work.creatorName} assigns ${work.handlerName}"
        itemView.work_item_create_date_text.text = fromDateToString(work.createTime)

        // show updatable view to work who manager is the creator or to admin
        presenter.setItemOnClick(user.roleId!!, user.userId!!, work)
        presenter.setUpMenu(work, user.userId!!, user.roleId!!)
        popupMenu.setOnMenuItemClickListener {
            popupMenu.dismiss()
            when (it.itemId) {
                Work.DELETED -> {
                    val builder = AlertDialog.Builder(itemView.context)
                    builder.apply {
                        setMessage("Do you want to delete this work")
                        setPositiveButton("Yes") { dialog, _ ->
                            presenter.deleteWork(work, position, user)
                            dialog.dismiss()
                        }
                        setNegativeButton("No", null)
                    }
                    builder.show()
                }
                Work.DECLINE -> showConfirmDialogAndUpdateStatus(
                    itemView,
                    work,
                    position,
                    "decline",
                    presenter
                )
                Work.CREATED_CONFIRMED -> showConfirmDialogAndUpdateStatus(
                    itemView,
                    work,
                    position,
                    "confirm create",
                    presenter
                )
                Work.IMPLEMENTING -> showConfirmDialogAndUpdateStatus(
                    itemView,
                    work,
                    position,
                    "start",
                    presenter
                )
                Work.FAILED -> {
                    val fragmentManager =
                        (itemView.context as AppCompatActivity).supportFragmentManager
                    UserFinishDialogFragment(work.apply {
                        work.status = Work.FAILED
                    }){ work, imgBitmap ->
                        presenter.finishWork(work, position, imgBitmap)
                    }.show(fragmentManager, null)
                }
                Work.DONE -> {
                    val fragmentManager =
                        (itemView.context as AppCompatActivity).supportFragmentManager
                    UserFinishDialogFragment(work.apply {
                        work.status = Work.DONE
                    }){ work, imgBitmap ->
                        presenter.finishWork(work, position, imgBitmap)
                    }.show(fragmentManager, null)
                }
                Work.FAILED_CONFIRMED -> {
                    work.status = Work.FAILED_CONFIRMED
                    showConfirmFinishDialogAndUpdateStatus(
                        itemView,
                        work,
                        position,
                        "Confirm fail",
                        presenter
                    )
                }
                Work.DONE_CONFIRMED -> {
                    work.status = Work.DONE_CONFIRMED
                    showConfirmFinishDialogAndUpdateStatus(
                        itemView,
                        work,
                        position,
                        "Confirm done",
                        presenter
                    )
                }
                else -> false
            }
            true
        }
        itemView.work_menu_btn.setOnClickListener {
            popupMenu.show()
        }
    }

    private fun showConfirmDialogAndUpdateStatus(
        view: View,
        work: Work,
        position: Int,
        message: String,
        presenter: WorkItemPresenter
    ): Boolean {
        val builder = AlertDialog.Builder(view.context)
        builder.apply {
            setMessage("Do you want to $message this work?")
            setPositiveButton("Yes") { dialog, _ ->
                presenter.updateWork(work, position, user)
                dialog.dismiss()
            }
            setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.show()
        return true
    }

    private fun showConfirmFinishDialogAndUpdateStatus(
        view: View,
        work: Work,
        position: Int,
        title: String,
        presenter: WorkItemPresenter
    ): Boolean {
        val builder = AlertDialog.Builder(view.context)
        val dialogView = View.inflate(view.context, R.layout.manager_finish_confirm_dialog, null)
        builder.apply {
            setView(dialogView)
            setTitle(title)
            setPositiveButton("Yes") { dialog, _ ->
                val comment = dialogView.manager_finish_comment_text.text.toString()
                val rating = dialogView.manager_finish_rating_bar.rating.toInt()
                work.apply {
                    managerComment = comment
                    managerRate = rating
                    managerCommentTime = Calendar.getInstance().time
                }
                presenter.updateWork(work, position, user)
                dialog.dismiss()
            }
            setNegativeButton("No") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.show()
        return true
    }
}