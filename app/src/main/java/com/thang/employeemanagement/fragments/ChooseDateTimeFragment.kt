package com.thang.employeemanagement.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment

import com.thang.employeemanagement.R
import kotlinx.android.synthetic.main.fragment_choose_date_time.*
import kotlinx.android.synthetic.main.fragment_choose_date_time.view.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ChooseDateTimeFragment(val dateTime: Calendar = Calendar.getInstance(), val setDateTime: (Calendar) -> Unit) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_choose_date_time, container, false)
        view.date_picker.updateDate(dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH))
        view.time_picker.currentHour = dateTime.get(Calendar.HOUR)
        view.time_picker.currentMinute = dateTime.get(Calendar.MINUTE)
        view.choose_date_time_btn.setOnClickListener {
            setDateTime(GregorianCalendar(
                date_picker.year,
                date_picker.month,
                date_picker.dayOfMonth,
                time_picker.currentHour,
                time_picker.currentMinute
            ))
            dismiss()
        }
        return view
    }

}
