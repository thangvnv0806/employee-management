package com.thang.employeemanagement.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.networking.dtos.Role
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.presenters.CreateUserPresenter
import kotlinx.android.synthetic.main.fragment_create_user.*
import kotlinx.android.synthetic.main.fragment_create_user.view.*

class CreateUserFragment : Fragment(), CreateUserPresenter.View {
    lateinit var fragmentView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_create_user, container, false)
        val presenter = CreateUserPresenter(this)

        presenter.loadRoleAndManager()

        fragmentView.create_user_submit_btn.setOnClickListener {
            presenter.createUser(
                User(
                    create_user_username_edit.text.toString(),
                    create_user_password_edit.text.toString(),
                    create_user_fullname_edit.text.toString(),
                    (create_user_role_spinner.selectedItem as Role).roleId,
                    managerId = if (create_user_manager_spinner.selectedItem != null) {
                        (create_user_manager_spinner.selectedItem as User).userId
                    } else null
                ), create_user_password_validate_edit.text.toString()
            )
        }
        return fragmentView
    }

    override fun onUserCreated() {
        findNavController().navigate(R.id.action_createUserFragment_to_userFragment)
    }

    override fun onRolesLoaded(roles: MutableList<Role>) {
        val roleAdapter = ArrayAdapter<Role>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            roles
        )
        view!!.create_user_role_spinner.apply {
            adapter = roleAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val managerSpinner = fragmentView.create_user_manager_spinner
                    if (!managerSpinner.adapter.isEmpty) {
                        val role = adapter.getItem(position) as Role
                        if (role.roleId == "employee") {
                            managerSpinner.isEnabled = true
                        } else {
                            managerSpinner.setSelection(0)
                            managerSpinner.isEnabled = false
                        }
                    }
                }
            }
        }
    }

    override fun onManagersLoaded(managers: MutableList<User>) {
        managers.add(0, User(userId = null))
        val managerAdapter = ArrayAdapter<User>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            managers
        )
        view!!.create_user_manager_spinner.adapter = managerAdapter
    }

    override fun onError(error: String) {
        view?.create_user_error_text?.text = error
    }
}
