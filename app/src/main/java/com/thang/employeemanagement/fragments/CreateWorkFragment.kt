package com.thang.employeemanagement.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.helpers.fromDateToString
import com.thang.employeemanagement.helpers.getFullnameFromSharedPref
import com.thang.employeemanagement.helpers.getRoleFromSharedPref
import com.thang.employeemanagement.helpers.getUserIdFromSharedPref
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.presenters.CreateWorkPresenter
import kotlinx.android.synthetic.main.fragment_create_work.view.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CreateWorkFragment : Fragment(), CreateWorkPresenter.View {

    private val handlerAdapter: ArrayAdapter<User> by lazy {
        ArrayAdapter<User>(requireContext(), android.R.layout.simple_spinner_item)
    }
    private val sourceAdapter: ArrayAdapter<Work> by lazy {
        ArrayAdapter<Work>(requireContext(), android.R.layout.simple_spinner_item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val presenter = CreateWorkPresenter(this)
        // Inflate the layout for this fragment
        val userId = getUserIdFromSharedPref()!!
        val fullname = getFullnameFromSharedPref()!!
        val roleId = getRoleFromSharedPref()!!

        val view = inflater.inflate(R.layout.fragment_create_work, container, false)
        var deadline: Calendar? = null
        view.create_work_creator_edit.setText(fullname)
        view.create_work_choose_deadline_time_btn.setOnClickListener {
            ChooseDateTimeFragment(deadline?: Calendar.getInstance()) {
                deadline = it
                view.create_work_deadline_time_text.text = fromDateToString(deadline?.time)
            }.show(requireActivity().supportFragmentManager, null)
        }
        view.create_work_submit_btn.setOnClickListener {
            val work = Work(
                workName = view.create_work_name_edit.text.toString(),
                sourceId = if (view.create_work_source_spinner.selectedItem != null) (view.create_work_source_spinner.selectedItem as Work).sourceId else null,
                description = view.create_work_description_edit.text.toString(),
                handleDescription = view.create_work_handle_description_edit.text.toString(),
                creatorId = userId,
                handlerId = (view.create_work_handler_spinner.selectedItem as User).userId,
                deadline = deadline?.time,
                createTime = Calendar.getInstance().time
            )
            work.status = if (userId == work.handlerId) Work.CREATED else Work.CREATED_CONFIRMED
            presenter.createWork(work, fullname, userId, roleId)
        }
        presenter.getSourcesAndHandlers(roleId, userId, fullname)
        return view
    }

    override fun onWorkCreate() {
        findNavController().navigate(R.id.action_createWorkFragment_to_workLoadFragment)
    }

    override fun onGetSources(sources: MutableList<Work>) {
        sourceAdapter.add(Work(workId = null, workName = null))
        sourceAdapter.addAll(sources)
        view!!.create_work_source_spinner.adapter = sourceAdapter
    }

    override fun onGetHandlers(handlers: List<User>) {
        handlerAdapter.addAll(handlers)
        view!!.create_work_handler_spinner.adapter = handlerAdapter
    }

    override fun onError(message: String) {
        view?.create_work_error_text?.text = message
    }
}
