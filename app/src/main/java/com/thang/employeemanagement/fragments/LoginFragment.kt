package com.thang.employeemanagement.fragments


import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.helpers.getPasswordFromSharedPref
import com.thang.employeemanagement.helpers.getSharedPref
import com.thang.employeemanagement.helpers.getUsernameFromSharedPref
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.presenters.LoginPresenter
import kotlinx.android.synthetic.main.fragment_login.view.*


/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment(), LoginPresenter.View {

    private lateinit var fragmentView: View
    private val presenter by lazy { LoginPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val username = getUsernameFromSharedPref()
        val password = getPasswordFromSharedPref()
        if (!username.isNullOrEmpty()) presenter.login(User(username, password), false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_login, container, false)
        fragmentView.login_login_btn.setOnClickListener {
            val username = fragmentView.login_username_edit.text.toString()
            val password = fragmentView.login_password_edit.text.toString()
            val user = User(username, password)
            presenter.login(user, true)
        }
        return fragmentView
    }

    override fun onLogin(user: User) {
        val sharedPref = getSharedPref()
        sharedPref?.edit().apply {
            this?.putString(getString(R.string.saved_username_key), user.username)
            this?.putString(getString(R.string.saved_fullname_key), user.fullName)
            this?.putString(getString(R.string.saved_password_key), user.password)
            this?.putInt(getString(R.string.saved_user_id_key), user.userId!!)
            this?.putString(getString(R.string.saved_user_role_key), user.roleId)
            this?.apply()
        }
        // hide soft keyboard
        view?.let {
            if (it.login_username_edit.isFocused || it.login_password_edit.isFocused) {
                val inputMethodManager: InputMethodManager =
                    activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(
                    activity?.currentFocus!!.windowToken,
                    0
                )
            }
        }
        findNavController().navigate(R.id.action_loginFragment_to_workLoadFragment)
    }

    override fun onError(message: String) {
        view?.login_error_text?.text = message
    }
}
