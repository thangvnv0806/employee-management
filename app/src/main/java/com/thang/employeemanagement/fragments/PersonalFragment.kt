package com.thang.employeemanagement.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.zxing.integration.android.IntentIntegrator
import com.thang.employeemanagement.R
import com.thang.employeemanagement.helpers.getRoleFromSharedPref
import com.thang.employeemanagement.helpers.getSharedPref
import com.thang.employeemanagement.helpers.getUserIdFromSharedPref
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.presenters.PersonalPresenter
import kotlinx.android.synthetic.main.fragment_personal.view.*


/**
 * A simple [Fragment] subclass.
 */
class PersonalFragment : Fragment(), PersonalPresenter.View {

    lateinit var fragmentView: View
    lateinit var roleId: String
    private val presenter by lazy { PersonalPresenter(this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_personal, container, false)
        val userId = getUserIdFromSharedPref()!!
        roleId = getRoleFromSharedPref()!!
        fragmentView.personal_log_out_btn.setOnClickListener {
            getSharedPref()?.edit()?.clear()?.apply()
            presenter.logOut(userId)
            findNavController().navigate(R.id.action_personalFragment_to_loginFragment)
        }
        if (roleId == "employee") fragmentView.personal_scan_btn.visibility = View.GONE
        else {
            fragmentView.personal_scan_btn.setOnClickListener {
                IntentIntegrator(requireActivity()).initiateScan()
            }
        }
        presenter.loadUser(userId)
        return fragmentView
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            result.contents?.let {
                try {
                    presenter.getUserFromScanner(it.toInt(), roleId)
                } catch (exception: Exception) {
                    fragmentView.personal_error_text.text = "Invalid User Id"
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onUserGotFromScanner(user: User, command: Int) {
        findNavController().navigate(
            R.id.action_personalFragment_to_updateUserFragment,
            bundleOf("user" to user, "command" to command)
        )
    }

    override fun onError() {
        fragmentView.personal_error_text.text = "User Id is not in database"
    }

    override fun onUserLoaded(user: User, qrBitmap: Bitmap) {
        user.let {
            fragmentView.personal_user_fullname_text.text = it.fullName
            fragmentView.personal_user_username_text.text = it.username
            fragmentView.personal_user_manager_text.text = it.managerName
            fragmentView.personal_user_role_text.text = it.roleName
            fragmentView.personal_qr_code_image.setImageBitmap(qrBitmap)
        }
    }
}
