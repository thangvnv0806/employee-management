package com.thang.employeemanagement.fragments

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.adapters.UserAdapter
import com.thang.employeemanagement.networking.dtos.Role
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.presenters.UpdateUserPresenter
import kotlinx.android.synthetic.main.fragment_update_user.view.*

class UpdateUserFragment : Fragment(), UpdateUserPresenter.View {

    val user by lazy { arguments?.getSerializable("user") as User }
    lateinit var fragmentView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentView = inflater.inflate(R.layout.fragment_update_user, container, false)
        val isUpdatable = arguments?.getInt("command") == UserAdapter.UPDATE_COMMAND
        val presenter = UpdateUserPresenter(this)
        fragmentView.apply {
            update_user_fullname_edit.setText(user.fullName)
            update_user_username_edit.setText(user.username)
            update_user_username_edit.inputType = InputType.TYPE_NULL
            if (isUpdatable) {
                update_user_submit_btn.setOnClickListener {
                    user.apply {
                        fullName = update_user_fullname_edit.text.toString()
                        managerId =
                            update_user_manager_spinner.selectedItem?.let { (it as User).userId }
                        roleId = update_user_role_spinner.selectedItem?.let { (it as Role).roleId }
                        if (update_user_password_edit.text.toString().isNotEmpty()) password = update_user_password_edit.text.toString()
                    }
                    presenter.updateUser(user, update_user_password_validate_edit.text.toString())
                }
            } else {
                update_user_submit_btn.visibility = View.GONE
                update_user_fullname_edit.inputType = InputType.TYPE_NULL
                update_user_manager_spinner.isEnabled = false
                update_user_role_spinner.isEnabled = false
            }
            presenter.loadRoleAndManager()
        }
        return fragmentView
    }

    override fun onUserUpdated() {
        findNavController().navigate(R.id.action_updateUserFragment_to_userFragment)
    }

    override fun onRolesLoaded(roles: MutableList<Role>) {
        val roleAdapter =
            ArrayAdapter<Role>(
                requireContext(), android.R.layout.simple_spinner_item,
                roles
            )
        view!!.update_user_role_spinner.apply {
            adapter = roleAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val managerSpinner = fragmentView.update_user_manager_spinner
                    if (!managerSpinner.adapter.isEmpty) {
                        val role = adapter.getItem(position) as Role
                        if (role.roleId == "employee") {
                            managerSpinner.isEnabled = true
                        } else {
                            managerSpinner.setSelection(0)
                            managerSpinner.isEnabled = false
                        }
                    }
                }
            }
            setSelection(roles.indexOfFirst { it.roleId == user.roleId })
        }
    }

    override fun onManagersLoaded(managers: MutableList<User>) {
        managers.add(0, User(userId = null))
        val managersAdapter = ArrayAdapter<User>(
            requireContext(), android.R.layout.simple_spinner_item,
            managers
        )
        view!!.apply {
            update_user_manager_spinner.adapter = managersAdapter
            update_user_manager_spinner.setSelection(managers.indexOfFirst { it.userId == user.managerId })
        }
    }

    override fun onError(error: String) {
        view?.update_user_error_text?.text = error
    }
}