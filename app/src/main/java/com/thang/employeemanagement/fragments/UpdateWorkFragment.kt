package com.thang.employeemanagement.fragments

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.adapters.WorkAdapter
import com.thang.employeemanagement.helpers.fromDateToString
import com.thang.employeemanagement.helpers.getFullnameFromSharedPref
import com.thang.employeemanagement.helpers.getRoleFromSharedPref
import com.thang.employeemanagement.helpers.getUserIdFromSharedPref
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.presenters.UpdateWorkPresenter
import kotlinx.android.synthetic.main.fragment_update_work.*
import kotlinx.android.synthetic.main.fragment_update_work.view.*
import java.util.*

class UpdateWorkFragment : Fragment(), UpdateWorkPresenter.View {

    private val handlerAdapter: ArrayAdapter<User> by lazy {
        ArrayAdapter<User>(requireContext(), android.R.layout.simple_spinner_item)
    }
    private val sourceAdapter: ArrayAdapter<Work> by lazy {
        ArrayAdapter<Work>(requireContext(), android.R.layout.simple_spinner_item)
    }
    private val work: Work by lazy {
        arguments!!["work"] as Work
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_update_work, container, false)
        val userId = getUserIdFromSharedPref()!!
        val fullname = getFullnameFromSharedPref()!!
        val roleId = getRoleFromSharedPref()!!
        val isUpdatable = arguments!!["command"] == WorkAdapter.UPDATE_COMMAND
        val presenter = UpdateWorkPresenter(this)
        view.run {
            if (work.status != Work.DONE && work.status != Work.FAILED) work_complete_part.visibility = View.GONE
            update_work_deadline_time_text.inputType = InputType.TYPE_NULL

            update_work_creator_edit.setText(work.creatorName)
            update_work_deadline_time_text.setText(fromDateToString(work.deadline))
            update_work_description_edit.setText(work.description)
            update_work_handle_description_edit.setText(work.handleDescription)
            update_work_manager_comment_edit.setText(work.managerComment)
            update_work_manager_comment_time_edit.text = fromDateToString(work.managerCommentTime)
            update_work_manager_rate_bar.rating = work.managerRate?.toFloat() ?: 0f
            update_work_name_edit.setText(work.workName)
            update_work_status.text = when (work.status) {
                Work.CREATED -> "CREATED - NEED CONFIRMATION"
                Work.IMPLEMENTING -> "IMPLEMENTING"
                Work.DONE -> "DONE - NEED CONFIRMATION"
                Work.DONE_CONFIRMED -> "COMPLETED"
                Work.FAILED -> "FAILED"
                Work.FAILED_CONFIRMED -> "FAILED - NEED CONFIRMATION"
                Work.DECLINE -> "DECLINE"
                Work.CREATED_CONFIRMED -> "CREATED - CONFIRMED"
                else -> ""
            }
            update_work_create_time_edit.text = fromDateToString(work.createTime)
            update_work_start_time_edit.text = fromDateToString(work.startTime)
            update_work_end_time_edit.text = fromDateToString(work.endTime)

            if (!isUpdatable) {
                update_work_source_spinner.isEnabled = false
                update_work_handler_spinner.isEnabled = false
                update_work_name_edit.inputType = InputType.TYPE_NULL
                update_work_description_edit.inputType = InputType.TYPE_NULL
                update_work_manager_comment_edit.inputType = InputType.TYPE_NULL
                update_work_handle_description_edit.inputType = InputType.TYPE_NULL
                update_work_manager_comment_edit.inputType = InputType.TYPE_NULL
                update_work_manager_rate_bar.setIsIndicator(false)
                update_work_submit_btn.visibility = View.GONE
            }
            if (isUpdatable) {
                update_work_choose_deadline_time_btn.setOnClickListener {
                    ChooseDateTimeFragment(Calendar.getInstance().apply { time = work.deadline!! }) {
                        view.update_work_deadline_time_text.setText(fromDateToString(it.time))
                        work.deadline = it.time
                    }.show(requireActivity().supportFragmentManager, null)
                }
            }
        }
        view.update_work_submit_btn.setOnClickListener {
            work.apply {
                workName = view.update_work_name_edit.text.toString()
                view.update_work_source_spinner.selectedItem?.let {
                    sourceId = (it as Work).workId
                }
                description = view.update_work_description_edit.text.toString()
                handleDescription = view.update_work_handle_description_edit.text.toString()
                managerRate = view.update_work_manager_rate_bar.rating.toInt()
                view.update_work_manager_comment_edit.text.toString().also {
                    if (work.managerComment?.let { comment -> it.contentEquals(comment) } == true) {
                        managerComment = it
                        managerCommentTime = Calendar.getInstance().time
                    }
                }
                handlerId = (update_work_handler_spinner.selectedItem as User).userId
            }
            presenter.updateWork(work, fullname, userId)
        }
        presenter.getSourcesAndHandlers(work, roleId, userId, fullname)

        var deadline: Calendar
        view.update_work_choose_deadline_time_btn.setOnClickListener {
            ChooseDateTimeFragment {
                deadline = it
                view.update_work_deadline_time_text.setText(fromDateToString(deadline.time))
            }.show(requireActivity().supportFragmentManager, null)
        }
        return view
    }

    override fun onWorkUpdate() {
        findNavController().navigate(R.id.action_updateWorkFragment_to_workLoadFragment)
    }

    override fun onGetHandlers(handlers: List<User>) {
        handlerAdapter.addAll(handlers)
        view.apply {
            update_work_handler_spinner.adapter = handlerAdapter
            update_work_handler_spinner.setSelection(handlers.indexOfFirst { it.userId == work.handlerId })
        }
    }

    override fun onError(message: String) {
        view?.update_work_error_text?.text = message
    }

    override fun onGetSources(sources: MutableList<Work>) {
        sourceAdapter.add(Work(workId = null, workName = null))
        sourceAdapter.addAll(sources)
        update_work_source_spinner.adapter = sourceAdapter
        update_work_source_spinner.setSelection(sources.indexOfFirst { it.workId == work.sourceId })
    }
}