package com.thang.employeemanagement.fragments

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.fragment.app.DialogFragment
import com.thang.employeemanagement.R
import com.thang.employeemanagement.helpers.fromDateToString
import com.thang.employeemanagement.helpers.getUsernameFromSharedPref
import com.thang.employeemanagement.networking.dtos.Work
import kotlinx.android.synthetic.main.user_finish_dialog.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*


class UserFinishDialogFragment(val work: Work, val onPositiveButtonCLick: (Work, Bitmap) -> Unit) :
    DialogFragment() {
    private val REQUEST_IMAGE_CAPTURE = 1
    private val dialogView: View by lazy {
        requireActivity().layoutInflater.inflate(
            R.layout.user_finish_dialog,
            null
        )
    }
    lateinit var bitmap: Bitmap

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return requireActivity().let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            dialogView.user_finish_upload_image_btn.setOnClickListener {
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                    }
                }
            }
            val username = getUsernameFromSharedPref()
            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Upload") { dialog, _ ->
                    MainScope().launch {
                        val currentTime = fromDateToString(Calendar.getInstance().time)
                        val imgName = "$username-${work.workId}-$currentTime"
                        work.apply {
                            endTime = Calendar.getInstance().time
                            verifyingImage = imgName
                        }
                        onPositiveButtonCLick(work, bitmap)
                    }
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
            builder.create()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            bitmap = data?.extras?.get("data") as Bitmap
            dialogView.user_finish_image.setImageBitmap(bitmap)
        }
    }
}