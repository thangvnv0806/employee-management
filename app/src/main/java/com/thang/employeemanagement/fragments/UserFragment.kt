package com.thang.employeemanagement.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.adapters.UserAdapter
import com.thang.employeemanagement.helpers.getRoleFromSharedPref
import com.thang.employeemanagement.helpers.getUserIdFromSharedPref
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.presenters.UserPresenter
import kotlinx.android.synthetic.main.fragment_user.view.*

class UserFragment : Fragment(), UserPresenter.View {

    private val userDao: UserDao by lazy { UserDao() }
    private lateinit var adapter: UserAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val userId = getUserIdFromSharedPref()!!
        val roleId = getRoleFromSharedPref()!!
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_user, container, false)
        val presenter = UserPresenter(this)
        adapter = UserAdapter(ArrayList(), roleId) { command, user ->
            if (command == UserAdapter.DELETE_COMMAND) {
                presenter.deleteUser(user)
            } else {
                val bundle = bundleOf("user" to user, "command" to command)
                findNavController().navigate(
                    R.id.action_userFragment_to_updateUserFragment,
                    bundle
                )
            }
        }
        presenter.loadUsers(roleId, userId)
        view.add_user_fab?.setOnClickListener {
            findNavController().navigate(R.id.action_userFragment_to_createUserFragment)
        }
        return view
    }

    override fun onUserDeleted() {
        adapter.notifyDataSetChanged()
    }

    override fun onUserLoaded(users: MutableList<User>) {
        adapter.users.addAll(users)
        view!!.user_rcv.adapter = adapter
    }
}
