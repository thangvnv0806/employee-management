package com.thang.employeemanagement.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.thang.employeemanagement.R
import com.thang.employeemanagement.adapters.WorkAdapter
import com.thang.employeemanagement.helpers.getFullnameFromSharedPref
import com.thang.employeemanagement.helpers.getRoleFromSharedPref
import com.thang.employeemanagement.helpers.getUserIdFromSharedPref
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.presenters.WorkLoadPresenter
import kotlinx.android.synthetic.main.fragment_work_load.view.*

/**
 * A simple [Fragment] subclass.
 */
class WorkLoadFragment : Fragment(), WorkLoadPresenter.View {

    val roleId by lazy {  getRoleFromSharedPref()!! }
    val userId by lazy { getUserIdFromSharedPref()!! }
    val fullname by lazy {getFullnameFromSharedPref()!! }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_work_load, container, false)
        view.welcome_text?.text = "Hello " + getFullnameFromSharedPref()

        view.add_work_fab?.setOnClickListener {
            findNavController().navigate(R.id.action_workLoadFragment_to_createWorkFragment)
        }
        val presenter = WorkLoadPresenter(this)
        presenter.getWorks(roleId, userId)
        return view
    }

    override fun onGetEmptyWork() {
        view!!.work_empty_text.visibility = View.INVISIBLE
    }

    override fun onGetWorks(works: MutableList<Work>) {
        val adapter = WorkAdapter(
            works,
            User(userId = userId, roleId = roleId, fullName = fullname)
        ) { command, work ->
            val bundle = bundleOf("work" to work, "command" to command)
            findNavController().navigate(
                R.id.action_workLoadFragment_to_updateWorkFragment,
                bundle
            )
        }
        view!!.work_load_rcv.adapter = adapter
    }
}
