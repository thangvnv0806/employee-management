package com.thang.employeemanagement.helpers

import java.text.SimpleDateFormat
import java.util.*

val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
fun fromDateToString(date: Date?): String {
    return if (date != null) dateFormat.format(date) else ""
}
fun fromStringToDate(dateTimeString: String): Date {
    return dateFormat.parse(dateTimeString)
}
fun fromDateToSqlTimeStamp(date: Date?): java.sql.Timestamp? {
    return date?.time?.let { java.sql.Timestamp(it) }
}