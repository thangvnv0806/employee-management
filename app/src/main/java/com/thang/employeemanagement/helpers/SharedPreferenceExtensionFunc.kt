package com.thang.employeemanagement.helpers

import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import com.thang.employeemanagement.R

fun Fragment.getSharedPref() : SharedPreferences? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )
}


fun Fragment.getUserIdFromSharedPref() : Int? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )?.getInt(getString(R.string.saved_user_id_key), -1)
}

fun Fragment.getUsernameFromSharedPref() : String? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )?.getString(getString(R.string.saved_username_key), "")
}

fun Fragment.getPasswordFromSharedPref() : String? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )?.getString(getString(R.string.saved_password_key), "")
}

fun Fragment.getFullnameFromSharedPref() : String? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )?.getString(getString(R.string.saved_fullname_key), "")
}

fun Fragment.getRoleFromSharedPref() : String? {
    return activity?.getSharedPreferences(
        getString(R.string.preference_file_key),
        Context.MODE_PRIVATE
    )?.getString(getString(R.string.saved_user_role_key), "")
}