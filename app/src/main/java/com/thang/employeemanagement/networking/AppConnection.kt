package com.thang.employeemanagement.networking

import java.lang.Exception
import java.sql.Connection
import java.sql.DriverManager

class AppConnection {

    companion object {
        fun getConnection(): Connection? {
            var connection: Connection? = null
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver")
                val serverIp = "192.168.1.49"
                val port = "1433"
                val databaseName = "EmployeeManagement"
                val username = "sa"
                val password = "123456"
                val connUrl = "jdbc:jtds:sqlserver://" +
                        "$serverIp:$port;" +
                        "databaseName=$databaseName;" +
                        "user=$username;" +
                        "password=$password;"
                connection = DriverManager.getConnection(connUrl)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return connection
        }
    }
}