package com.thang.employeemanagement.networking.daos

import com.thang.employeemanagement.networking.AppConnection
import com.thang.employeemanagement.networking.dtos.Role
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

class RoleDao {
    private var connection: Connection? = null
    private var statement: PreparedStatement? = null
    private var resultSet: ResultSet? = null

    suspend fun getRoles(): MutableList<Role> = withContext(Dispatchers.IO) {
        val result = ArrayList<Role>()
        connection =
            AppConnection.getConnection()
        try {
            val sql = "Select roleId, roleName from Role "
            statement = connection?.prepareStatement(sql)
            resultSet = statement?.executeQuery()?.apply {
                while (next()) {
                    val roleId = getString("roleId").trim()
                    val roleName = getString("roleName")
                    result.add(Role(roleId, roleName))
                }
            }
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    private fun closeConnection() {
        resultSet?.close()
        statement?.close()
        connection?.close()
    }
}