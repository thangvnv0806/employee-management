package com.thang.employeemanagement.networking.daos

import android.util.Log
import com.thang.employeemanagement.networking.AppConnection
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Types.INTEGER

class UserDao {
    private var connection: Connection? = null
    private var statement: PreparedStatement? = null
    private var resultSet: ResultSet? = null

    suspend fun login(user: User): User? = withContext(Dispatchers.IO) {
        var result: User? = null
        connection =
            AppConnection.getConnection()
        try {
            val sql = "Select userId, fullname, roleId, managerId from Users " +
                    "where username=? and password=? and isDelete='false'"
            statement = connection?.prepareStatement(sql)
            statement?.run {
                setString(1, user.username)
                setString(2, user.password)
            }
            resultSet = statement!!.executeQuery().apply {
                if (next()) {
                    val userId = getInt("userId")
                    val fullname = getString("fullname")
                    val roleId = getString("roleId").trim()
                    val managerId = getInt("managerId")
                    result = User(
                        user.username,
                        user.password,
                        fullname,
                        roleId,
                        managerId = managerId,
                        userId = userId
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getUsersOfAdmin(): MutableList<User> = withContext(Dispatchers.IO) {
        val result: ArrayList<User> = ArrayList()
        val connection =
            AppConnection.getConnection()
        try {
            val sql =
                "Select Users.userId, Users.username, Users.fullname, Users.roleId, Role.roleName, Users.managerId, Manager.fullname as managerName " +
                        "from Users, Role, (Select userId, fullname from Users where roleId = 'manager') as Manager " +
                        "where Users.roleId!='admin' " +
                        "and Users.roleId = Role.roleId " +
                        "and (Users.managerId = Manager.userId or Users.managerId  IS NULL)"
            statement = connection?.prepareStatement(sql)
            resultSet = statement?.executeQuery()?.apply {
                while (next()) {
                    val userId = getInt("userId")
                    val username = getString("username")
                    val fullname = getString("fullname")
                    val roleId = getString("roleId").trim()
                    val roleName = getString("roleName")
                    val managerId = getInt("managerId")
                    val managerName = getString("managerName")
                    result.add(
                        User(
                            username,
                            null,
                            fullname,
                            roleId,
                            roleName,
                            managerId,
                            managerName,
                            false,
                            userId
                        )
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getUsers(roleId: String): MutableList<User> = withContext(Dispatchers.IO) {
        val result: ArrayList<User> = ArrayList()
        val connection =
            AppConnection.getConnection()
        try {
            val sql =
                "Select Users.userId, Users.username, Users.fullname, Users.roleId, Role.roleName, Users.managerId, Manager.fullname as managerName " +
                        "from Users, Role, (Select userId, fullname from Users where roleId = 'manager') as Manager " +
                        "where Users.roleId!='admin' " +
                        "and Users.roleId = Role.roleId " +
                        "and (Users.managerId = Manager.userId or Users.managerId IS NULL) " +
                        "and Users.roleId=?"
            statement = connection?.prepareStatement(sql)
            statement?.setString(1, roleId)
            resultSet = statement!!.executeQuery().apply {
                while (next()) {
                    val userId = getInt("userId")
                    val username = getString("username")
                    val fullname = getString("fullname")
                    val roleName = getString("roleName")
                    val managerId = getInt("managerId")
                    val managerName = getString("managerName")
                    result.add(
                        User(
                            username,
                            null,
                            fullname,
                            roleId,
                            roleName,
                            managerId,
                            managerName,
                            false,
                            userId
                        )
                    )
                }
            }
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getUser(userId: Int): User? = withContext(Dispatchers.IO) {
        var result: User? = null
        val connection =
            AppConnection.getConnection()
        try {
            val sql =
                "Select Users.userId, Users.username, Users.fullname, Users.roleId, Role.roleName, Users.managerId, Manager.fullname as managerName " +
                        "from Users, Role, (Select userId, fullname from Users where roleId = 'manager') as Manager " +
                        "where Users.roleId = Role.roleId " +
                        "and (Users.managerId = Manager.userId or Users.managerId IS NULL) " +
                        "and Users.userId=?"
            statement = connection?.prepareStatement(sql)
            statement?.setInt(1, userId)
            resultSet = statement!!.executeQuery().apply {
                if (next()) {
                    val roleId = getString("roleId")
                    val username = getString("username")
                    val fullname = getString("fullname")
                    val roleName = getString("roleName")
                    val managerId = getInt("managerId")
                    val managerName = getString("managerName")
                    result = User(
                        username,
                        null,
                        fullname,
                        roleId,
                        roleName,
                        managerId,
                        managerName,
                        false,
                        userId
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getUserToken(userId: Int): String? = withContext(Dispatchers.IO) {
        var result: String? = null
        val connection = AppConnection.getConnection()
        try {
            val sql = "Select fcmToken from Users where userId=?"
            statement = connection?.prepareStatement(sql)
            statement?.setInt(1, userId)
            resultSet = statement!!.executeQuery().apply {
                if (next()) {
                    result = getString("fcmToken")
                }
            }
        } catch (e: Exception) {
            Log.e("getUserToken", e.toString())
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getUsersOfManager(userId: Int): MutableList<User> = withContext(Dispatchers.IO) {
        val result: ArrayList<User> = ArrayList()
        val connection =
            AppConnection.getConnection()
        try {
            val sql =
                "Select Users.userId, Users.username, Users.fullname, Users.roleId, Role.roleName, Users.managerId, Manager.fullname as managerName " +
                        "from Users, Role, (Select userId, fullname from Users where roleId = 'manager') as Manager " +
                        "where Users.roleId='employee' " +
                        "and Users.roleId = Role.roleId " +
                        "and Users.managerId = Manager.userId " +
                        "and Users.managerId = ?"
            statement = connection!!.prepareStatement(sql)
            statement?.setInt(1, userId)
            resultSet = statement!!.executeQuery().apply {
                while (next()) {
                    val username = getString("username")
                    val fullname = getString("fullname")
                    val roleId = getString("roleId").trim()
                    val roleName = getString("roleName")
                    val managerId = getInt("managerId")
                    val managerName = getString("managerName")
                    result.add(
                        User(
                            username,
                            null,
                            fullname,
                            roleId,
                            roleName,
                            managerId,
                            managerName,
                            false,
                            userId
                        )
                    )
                }
            }
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun insertUser(user: User): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection =
            AppConnection.getConnection()
        try {
            val sql =
                "Insert into Users(username, password, fullname, managerId, roleId, isDelete) " +
                        "values(?,?,?,?,?,?)"
            statement = connection!!.prepareStatement(sql)
            statement?.run {
                setString(1, user.username)
                setString(2, user.password)
                setString(3, user.fullName)
                if (user.managerId != null) setInt(4, user.managerId!!) else setNull(4, INTEGER)
                setString(5, user.roleId)
                setBoolean(6, false)
            }
            result = statement!!.executeUpdate() > 0
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun updateUser(user: User): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection =
            AppConnection.getConnection()
        try {
            val sql = "Update Users " +
                    "set fullname=?, managerId=?, roleId=? " +
                    "where userId=?"
            statement = connection!!.prepareStatement(sql)
            statement?.run {
                setString(1, user.fullName)
                if (user.managerId != null) setInt(2, user.managerId!!) else setNull(2, INTEGER)
                setString(3, user.roleId)
                setInt(4, user.userId!!)
            }
            result = statement!!.executeUpdate() > 0
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun updateFcmKey(userId: Int, token: String): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection =
            AppConnection.getConnection()
        try {
            val sql = "Update Users " +
                    "set fcmToken=? " +
                    "where userId=?"
            statement = connection!!.prepareStatement(sql)
            statement?.run {
                setString(1, token)
                setInt(2, userId)
            }
            result = statement!!.executeUpdate() > 0
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun deleteUser(user: User): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection =
            AppConnection.getConnection()
        try {
            val sql = "Update Users " +
                    "set isDelete=? " +
                    "where userId=?"
            statement = connection!!.prepareStatement(sql)
            statement?.run {
                setBoolean(1, true)
                setInt(2, user.userId!!)
            }
            result = statement!!.executeUpdate() > 0
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    private fun closeConnection() {
        resultSet?.close()
        statement?.close()
        connection?.close()
    }
}