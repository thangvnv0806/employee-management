package com.thang.employeemanagement.networking.daos

import com.thang.employeemanagement.helpers.fromDateToSqlTimeStamp
import com.thang.employeemanagement.networking.AppConnection
import com.thang.employeemanagement.networking.dtos.UpdateRecord
import com.thang.employeemanagement.networking.dtos.Work
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.sql.*
import java.sql.Types.INTEGER

class WorkDao {
    private var connection: Connection? = null
    private var statement: PreparedStatement? = null
    private var resultSet: ResultSet? = null

    suspend fun getWorks(): MutableList<Work> = withContext(Dispatchers.IO) {
        val result: MutableList<Work> = ArrayList()
        val connection = AppConnection.getConnection()
        try {
            val sql =
                "Select Work.workId, Work.sourceId, Work.workName, Work.descriptions, Work.handleDescription, " +
                        "Work.managerComment, Work.managerRate, Work.managerCommentTime, " +
                        "Work.startTime, Work.endTime, Work.deadline, Work.status, Work.createTime, " +
                        "Work.creatorId, Creator.fullname as creatorName, Work.handlerId, Handler.fullname as handlerName, Work.verifyingImage " +
                        "from Work, " +
                        "(Select userId, fullname from Users) as Creator, " +
                        "(Select userId, fullname from Users) as Handler " +
                        "where Work.isDelete = 'false' " +
                        "and Creator.userId=Work.creatorId " +
                        "and Handler.userId=Work.handlerId"
            statement = connection?.prepareStatement(sql)
            resultSet = statement?.executeQuery()?.apply {
                while (next()) {
                    val workId = getInt("workId")
                    val sourceId = getInt("sourceId")
                    val workName = getString("workName")
                    val description = getString("descriptions")
                    val handleDescription = getString("handleDescription")
                    val managerComment = getString("managerComment")
                    val managerRate = getInt("managerRate")
                    val managerCommentTime = getDate("managerCommentTime")
                    val startTime = getDate("startTime")
                    val endTime = getDate("endTime")
                    val deadline = getDate("deadline")
                    val createTime = getDate("createTime")
                    val status = getInt("status")
                    val handlerId = getInt("handlerId")
                    val handlerName = getString("handlerName")
                    val creatorId = getInt("creatorId")
                    val creatorName = getString("creatorName")
                    val verifyingImage = getString("verifyingImage")
                    result.add(
                        Work(
                            workName,
                            sourceId,
                            description,
                            handleDescription,
                            managerComment,
                            managerRate,
                            managerCommentTime,
                            startTime,
                            deadline,
                            endTime,
                            status,
                            createTime,
                            creatorId,
                            creatorName,
                            handlerId,
                            handlerName,
                            verifyingImage,
                            workId
                        )
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun getWorks(userId: Int): MutableList<Work> = withContext(Dispatchers.IO) {
        val result: MutableList<Work> = ArrayList()
        val connection = AppConnection.getConnection()
        try {
            val sql =
                "Select Work.workId, Work.sourceId, Work.workName, Work., Work.handleDescription, " +
                        "Work.managerComment, Work.managerRate, Work.managerCommentTime, " +
                        "Work.startTime, Work.endTime, Work.deadline, Work.status, Work.createTime, " +
                        "Work.creatorId, Creator.fullname as creatorName, Work.handlerId, Handler.fullname as handlerName, Work.verifyingImage " +
                        "from Work, " +
                        "(Select userId, fullname from Users) as Creator," +
                        "(Select userId, fullname from Users) as Handler " +
                        "where Work.isDelete = 'false' " +
                        "and Creator.userId=Work.creatorId " +
                        "and Handler.userId=Work.handlerId " +
                        "and (Creator.userId=? Or Handler.userId=?)"
            statement = connection?.prepareStatement(sql)?.apply {
                setInt(1, userId)
                setInt(2, userId)
            }
            resultSet = statement?.executeQuery()?.apply {
                while (next()) {
                    val workId = getInt("workId")
                    val sourceId = getInt("sourceId")
                    val workName = getString("workName")
                    val description = getString("descriptions")
                    val handleDescription = getString("handleDescription")
                    val managerComment = getString("managerComment")
                    val managerRate = getInt("managerRate")
                    val managerCommentTime = getDate("managerCommentTime")
                    val startTime = getDate("startTime")
                    val endTime = getDate("endTime")
                    val deadline = getDate("deadline")
                    val createTime = getDate("createTime")
                    val status = getInt("status")
                    val handlerId = getInt("handlerId")
                    val handlerName = getString("handlerName")
                    val creatorId = getInt("creatorId")
                    val creatorName = getString("creatorName")
                    val verifyingImage = getString("verifyingImage")
                    result.add(
                        Work(
                            workName,
                            sourceId,
                            description,
                            handleDescription,
                            managerComment,
                            managerRate,
                            managerCommentTime,
                            startTime,
                            deadline,
                            endTime,
                            status,
                            createTime,
                            creatorId,
                            creatorName,
                            handlerId,
                            handlerName,
                            verifyingImage,
                            workId
                        )
                    )
                }
            }
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun insertWork(work: Work): Int? = withContext(Dispatchers.IO) {
        var result: Int? = null
        val connection = AppConnection.getConnection()
        try {
            val sql = "Insert into Work(sourceId, workName, descriptions, handleDescription, " +
                    "managerComment, managerRate, managerCommentTime, " +
                    "startTime, endTime, deadline, status, createTime, " +
                    "creatorId, handlerId, verifyingImage, isDelete) " +
                    "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            statement = connection?.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)?.apply {
                if (work.sourceId != null) setInt(1, work.sourceId!!) else setNull(1, INTEGER)
                setString(2, work.workName)
                setString(3, work.description)
                setString(4, work.handleDescription)
                setString(5, work.managerComment)
                if (work.managerRate != null) setInt(6, work.managerRate!!) else setNull(6, INTEGER)
                setTimestamp(7, fromDateToSqlTimeStamp(work.managerCommentTime))
                setTimestamp(8, fromDateToSqlTimeStamp(work.startTime))
                setTimestamp(9, fromDateToSqlTimeStamp(work.endTime))
                setTimestamp(10, Timestamp(work.deadline!!.time))
                if (work.status != null) setInt(11, work.status!!) else setNull(11, INTEGER)
                setTimestamp(12, fromDateToSqlTimeStamp(work.createTime))
                setInt(13, work.creatorId!!)
                setInt(14, work.handlerId!!)
                setString(15, work.verifyingImage)
                setBoolean(16, false)
            }
            statement?.executeUpdate()
            resultSet = statement?.generatedKeys
            if (resultSet!!.next()) {
                result = resultSet!!.getInt(1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun updateWork(work: Work): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection = AppConnection.getConnection()
        try {
            val sql = "Update Work " +
                    "set sourceId=?, workName=?, descriptions=?, handleDescription=?, " +
                    "managerComment=?, managerRate=?, managerCommentTime=?, " +
                    "startTime=?, endTime=?, deadline=?, status=?, createTime=?, " +
                    "creatorId=?, handlerId=?, verifyingImage=? " +
                    "where workId=?"
            statement = connection?.prepareStatement(sql)?.apply {
                if (work.sourceId != null) setInt(1, work.sourceId!!) else setNull(1, INTEGER)
                setString(2, work.workName)
                setString(3, work.description)
                setString(4, work.handleDescription)
                setString(5, work.managerComment)
                if (work.managerRate != null) setInt(6, work.managerRate!!) else setNull(6, INTEGER)
                setTimestamp(7, fromDateToSqlTimeStamp(work.managerCommentTime))
                setTimestamp(8, fromDateToSqlTimeStamp(work.startTime))
                setTimestamp(9, fromDateToSqlTimeStamp(work.endTime))
                setTimestamp(10, fromDateToSqlTimeStamp(work.deadline))
                if (work.status != null) setInt(11, work.status!!) else setNull(11, INTEGER)
                setTimestamp(12, fromDateToSqlTimeStamp(work.createTime))
                setInt(13, work.creatorId!!)
                setInt(14, work.handlerId!!)
                setString(15, work.verifyingImage)
                setInt(16, work.workId!!)
            }
            result = statement?.executeUpdate()!! > 0
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun insertUpdateRecord(updateRecord: UpdateRecord): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection = AppConnection.getConnection()
        try {
            val sql = "Insert into UpdateRecord(updaterId, descriptions, updateTime) " +
                    "values(?,?,?)"
            statement = connection?.prepareStatement(sql)?.apply {
                setInt(1, updateRecord.updaterId)
                setString(2, updateRecord.description)
                setTimestamp(3, fromDateToSqlTimeStamp(updateRecord.updateTime))
            }
            result = statement?.executeUpdate()!! > 0
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            closeConnection()
        }
        result
    }

    suspend fun deleteWork(work: Work): Boolean = withContext(Dispatchers.IO) {
        var result = false
        val connection =
            AppConnection.getConnection()
        try {
            val sql = "Update Work " +
                    "set isDelete=? " +
                    "where workId=?"
            statement = connection!!.prepareStatement(sql)
            statement?.run {
                setBoolean(1, true)
                setInt(2, work.workId!!)
            }
            result = statement!!.executeUpdate() > 0
        } catch (e: Exception) {
        } finally {
            closeConnection()
        }
        result
    }

    private fun closeConnection() {
        resultSet?.close()
        statement?.close()
        connection?.close()
    }
}