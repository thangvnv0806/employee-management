package com.thang.employeemanagement.networking.dtos

data class Role(
    val roleId: String,
    val roleName: String
) {
    override fun toString(): String = roleName
}