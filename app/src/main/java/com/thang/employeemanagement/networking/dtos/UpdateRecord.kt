package com.thang.employeemanagement.networking.dtos

import java.util.*

data class UpdateRecord(
    var updateId: Int = -1,
    val updaterId: Int,
    val description: String,
    val updateTime: Date
)