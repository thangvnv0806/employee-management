package com.thang.employeemanagement.networking.dtos

import java.io.Serializable

data class User (
    var username: String? = null,
    var password: String? = null,
    var fullName: String? = null,
    var roleId: String? = null,
    var roleName: String? = null,
    var managerId: Int? = null,
    var managerName: String? = null,
    var delete: Boolean = false,
    var userId: Int? = 0
): Serializable {
    constructor(username: String, password: String) : this(
        username,
        password,
        null,
        null,
        null,
        null,
        null,
        false,
        0
    )

    override fun toString(): String = fullName?: ""
}