package com.thang.employeemanagement.networking.dtos

import java.io.Serializable
import java.util.*

data class Work(
    var workName: String? = null,
    var sourceId: Int? = null,
    var description: String? = null,
    var handleDescription: String? = null,
    var managerComment: String? = null,
    var managerRate: Int? = null,
    var managerCommentTime: Date? = null,
    var startTime: Date? = null,
    var deadline: Date? = null,
    var endTime: Date? = null,
    var status: Int? = 0,
    var createTime: Date? = null,
    var creatorId: Int? = null,
    val creatorName: String? = null,
    var handlerId: Int? = null,
    val handlerName: String? = null,
    var verifyingImage: String? = null, // username-id-endTime
    var workId: Int? = 0
): Serializable {
    override fun toString(): String = workName?: ""
    fun toStringData(): String = super.toString()

    companion object {
        const val DELETED = -2
        const val DECLINE = -1
        const val CREATED = 0
        const val CREATED_CONFIRMED = 1
        const val IMPLEMENTING = 3
        const val DONE = 4
        const val DONE_CONFIRMED = 5
        const val FAILED = 6
        const val FAILED_CONFIRMED = 7
        const val LATE = 8
    }
}