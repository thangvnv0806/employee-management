package com.thang.employeemanagement.networking.volleys

import android.content.Context
import android.util.Log
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class VolleyNetworkRequest(val context: Context) {
    private val FCM_API_SEND = "https://fcm.googleapis.com/fcm/send"
    private val FCM_API_SUBCRIBE = "https://iid.googleapis.com/iid/v1:batchAdd"
    private val serverKey =
        "key=" + "AAAAdMUQ4z8:APA91bFsIp9N9NP7fzo1-M6ptETfY_EOtYDJivMYoDmGvv0pXZzo1cwf05Et5HVSqjeNTTHZqoa3Bg9RIjcrspGNV3h6sM9e9QCTqoKjL712awaS8IciLRyt5_iyJXIfcks3WnoQOmDJ"
    private val contentType = "application/json"

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    companion object {
        @Volatile
        private var INSTANCE: VolleyNetworkRequest? = null

        fun getInstance(context: Context? = null): VolleyNetworkRequest =
            if (context != null) {
                INSTANCE ?: synchronized(this) {
                    VolleyNetworkRequest(context).also {
                        INSTANCE = it
                    }
                }
            } else {
                INSTANCE!!
            }
    }

    fun sendNotification(topic: String, message: String, workName: String) {
        val jsonObject = JSONObject().apply {
            put("to", "/topics/$topic")
            put("notification", JSONObject().apply {
                put("title", "Changes from work $workName")
                put("body", message)
            })
        }
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.POST, FCM_API_SEND, jsonObject,
            { response ->
                Log.i("TAG", "onResponse: $response")
            },
            {
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {
            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    fun subscribeToTopic(topic: String, vararg token: String, onSuccess: () -> Unit) {
        val jsonObject = JSONObject().apply {
            put("to", "/topics/$topic")
            put("registration_tokens", listOf(token))
        }
        Log.d("jsonObject", jsonObject.toString())
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.POST, FCM_API_SUBCRIBE, jsonObject,
            { response ->
                Log.i("TAG", "onResponse: $response")
                onSuccess()
            },
            {
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {
            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    fun sendImage(imageString: String, imageName: String) {
        val jsonObjectRequest = object : StringRequest(
            Method.POST, FCM_API_SEND,
            { response ->
                Log.i("TAG", "onResponse: $response")
            },
            {
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {
            override fun getParams(): MutableMap<String, String> {
                val map = HashMap<String, String>().apply {
                    put("imageString", imageString)
                    put("name", imageName)
                }
                return map
            }
        }
        requestQueue.add(jsonObjectRequest)
    }
}