package com.thang.employeemanagement.presenters

import com.thang.employeemanagement.networking.daos.RoleDao
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.dtos.Role
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class CreateUserPresenter(private val userCreateView: View) {
    fun createUser(user: User, passwordValidate: String) {
        var error = ""
        if (user.password!!.isEmpty()) error = "Password is empty"
        if (passwordValidate.isEmpty()) error = "Validate Password is empty"
        if (user.password!!.isNotEmpty() && passwordValidate.isNotEmpty() && !user.password.equals(passwordValidate)) error = "Validate Password and password does not match"
        if (user.username!!.isEmpty()) error = "Username is empty"
        if (user.fullName!!.isEmpty()) error = "Fullname is empty"
        if (error.isNotEmpty()) userCreateView.onError(error)
        else {
            MainScope().launch {
                UserDao().insertUser(user).also { isSuccessful ->
                    if (isSuccessful) userCreateView.onUserCreated()
                }
            }
        }
    }

    fun loadRoleAndManager() {
        MainScope().launch {
            val roles = RoleDao().getRoles()
            val managers = UserDao().getUsers("manager")

            userCreateView.onRolesLoaded(roles)
            userCreateView.onManagersLoaded(managers)
        }
    }

    interface View {
        fun onUserCreated()
        fun onRolesLoaded(roles: MutableList<Role>)
        fun onManagersLoaded(managers: MutableList<User>)
        fun onError(error: String)
    }
}