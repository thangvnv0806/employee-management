package com.thang.employeemanagement.presenters

import com.google.firebase.messaging.FirebaseMessaging
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.UpdateRecord
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.networking.volleys.VolleyNetworkRequest
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.util.*

class CreateWorkPresenter(private val createWorkView: View) {
    interface View {
        fun onWorkCreate()
        fun onGetSources(sources: MutableList<Work>)
        fun onGetHandlers(handlers: List<User>)
        fun onError(message: String)
    }

    private val workDao by lazy { WorkDao() }
    private val userDao by lazy { UserDao() }

    fun createWork(
        work: Work,
        fullName: String,
        userId: Int,
        roleId: String
    ) {
        var error = ""
        if (work.workName.isNullOrEmpty()) error = "Empty work name"
        if (work.deadline == null) error = "Undefined deadline"
        if (error.isEmpty()) createWorkView.onError(error)
        else {
            MainScope().launch {
                val workId = workDao.insertWork(work)
                if (workId != null) {
                    createWorkView.onWorkCreate()
                    workDao.insertUpdateRecord(
                        UpdateRecord(
                            updaterId = work.creatorId!!, updateTime = Calendar.getInstance().time,
                            description = "$fullName Create work ${work.workName}"
                        )
                    )
                    FirebaseMessaging.getInstance().subscribeToTopic(workId.toString())
                        .addOnSuccessListener {
                            MainScope().launch {
                                var token: String? = null
                                if (roleId == "employee") {
                                    token = UserDao().let { userDao ->
                                        userDao.getUser(userId)?.managerId?.let {
                                            userDao.getUserToken(it)
                                        }
                                    }
                                } else {
                                    if (userId != work.handlerId) {
                                        token = UserDao().getUserToken(work.handlerId!!)
                                    }
                                }
                                if (token != null) {
                                    VolleyNetworkRequest.getInstance().subscribeToTopic(
                                        workId.toString(), token
                                    ) {
                                        // onSuccess
                                        VolleyNetworkRequest.getInstance().sendNotification(
                                            workId.toString(),
                                            "$fullName Create work ${work.workName}",
                                            work.workName!!
                                        )
                                    }
                                } else {
                                    VolleyNetworkRequest.getInstance().sendNotification(
                                        workId.toString(),
                                        "$fullName Create work ${work.workName}",
                                        work.workName!!
                                    )
                                }
                            }
                        }
                }
            }
        }
    }

    fun getSourcesAndHandlers(roleId: String, userId: Int, fullname: String) {
        MainScope().launch {
            coroutineScope {
                val sources = workDao.getWorks()
                val handlers = when (roleId) {
                    "admin" -> userDao.getUsersOfAdmin()
                    "manager" -> userDao.getUsersOfManager(userId)
                    else -> listOf(User(userId = userId, fullName = fullname))
                }
                createWorkView.onGetSources(sources)
                createWorkView.onGetHandlers(handlers)
            }
        }
    }
}