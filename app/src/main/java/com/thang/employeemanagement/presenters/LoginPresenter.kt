package com.thang.employeemanagement.presenters

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class LoginPresenter(val loginView: View) {
    private val userDao: UserDao by lazy { UserDao() }

    interface View {
        fun onError(message: String)
        fun onLogin(user: User)
    }

    fun login(
        user: User,
        needSubscribe: Boolean
    ) {
        if (user.username.isNullOrEmpty() || user.password.isNullOrEmpty())
            loginView.onError("Username or password cannot be empty")
        else {
            MainScope().launch {
                userDao.login(user).also {
                    if (it != null) {
                        loginView.onLogin(it)
                        val works = WorkDao().getWorks(user.userId!!)
                        if (needSubscribe) {
                            FirebaseMessaging.getInstance().also { firebaseMessaging ->
                                for (work in works) {
                                    firebaseMessaging.subscribeToTopic(work.workId.toString())
                                }
                            }
                        }
                        FirebaseInstanceId.getInstance()
                            .instanceId.addOnSuccessListener { instanceIdResult ->
                            Log.d("onGetToken", "Token: " + instanceIdResult.token)
                            MainScope().launch {
                                UserDao().updateFcmKey(it.userId!!, instanceIdResult.token)
                            }
                        }
                    } else {
                        loginView.onError("Incorrect username or password")
                    }
                }
            }
        }
    }
}