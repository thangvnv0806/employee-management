package com.thang.employeemanagement.presenters

import android.graphics.Bitmap
import com.google.firebase.messaging.FirebaseMessaging
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.thang.employeemanagement.adapters.UserAdapter
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch

class PersonalPresenter(private val personalView: View) {
    fun getUserFromScanner(userId: Int, roleId: String) {
        MainScope().launch {
            val user = UserDao().getUser(userId)
            if (user != null) {
                // only admin can update other's info, but if the scanned image was of admin, he/she can only view
                val command = if (roleId == "admin" && user.roleId != "admin") UserAdapter.UPDATE_COMMAND else UserAdapter.VIEW_COMMAND
                personalView.onUserGotFromScanner(user, command)
            } else personalView.onError()
        }
    }

    fun loadUser(userId: Int) {
        MainScope().launch {
            val user = UserDao().getUser(userId)
            user?.let {
                val qrBitmap = generateQrCode(it.userId.toString())
                personalView.onUserLoaded(user, qrBitmap)
            }
        }
    }

    private suspend fun generateQrCode(inputValue: String): Bitmap = Dispatchers.Default {
        val barcodeEncoder = BarcodeEncoder()
        val bitmap = barcodeEncoder.encodeBitmap(inputValue, BarcodeFormat.QR_CODE, 400, 400)
        bitmap
    }

    fun logOut(userId: Int) {
        MainScope().launch {
            val works = WorkDao().getWorks(userId)
            FirebaseMessaging.getInstance().also {
                for (work in works) {
                    it.unsubscribeFromTopic(work.workId.toString())
                }
            }
            UserDao().updateFcmKey(userId, "")
        }
    }

    interface View {
        fun onUserGotFromScanner(user: User, command: Int)
        fun onError()
        fun onUserLoaded(
            user: User,
            qrBitmap: Bitmap
        )
    }
}