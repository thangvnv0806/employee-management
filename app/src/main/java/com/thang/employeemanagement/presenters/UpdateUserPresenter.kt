package com.thang.employeemanagement.presenters

import com.thang.employeemanagement.networking.daos.RoleDao
import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.dtos.Role
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class UpdateUserPresenter(private val userUpdateView: View) {
    fun updateUser(user: User, passwordValidate: String) {
        var error = ""
        if (user.username!!.isEmpty()) error = "Username is empty"
        if (user.fullName!!.isEmpty()) error = "Fullname is empty"
        if (!user.password.isNullOrEmpty() && passwordValidate.isNotEmpty() && !user.password.equals(passwordValidate)) error = "Validate Password and password does not match"
        if (error.isNotEmpty()) userUpdateView.onError(error)
        MainScope().launch {
            UserDao().updateUser(user).also { isSuccessful ->
                if (isSuccessful) userUpdateView.onUserUpdated()
            }
        }
    }

    fun loadRoleAndManager() {
        MainScope().launch {
            val roles = RoleDao().getRoles()
            val managers = UserDao().getUsers("manager")

            userUpdateView.onRolesLoaded(roles)
            userUpdateView.onManagersLoaded(managers)
        }
    }

    interface View {
        fun onUserUpdated()
        fun onRolesLoaded(roles: MutableList<Role>)
        fun onManagersLoaded(managers: MutableList<User>)
        fun onError(error: String)
    }
}