package com.thang.employeemanagement.presenters

import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.UpdateRecord
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.networking.volleys.VolleyNetworkRequest
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.util.*

class UpdateWorkPresenter(val updateWorkView: View) {
    interface View {
        fun onWorkUpdate()
        fun onGetSources(sources: MutableList<Work>)
        fun onGetHandlers(handlers: List<User>)
        fun onError(message: String)
    }

    private val workDao by lazy { WorkDao() }
    private val userDao by lazy { UserDao() }

    fun updateWork(
        work: Work,
        fullName: String,
        updaterId: Int
    ) {
        if (work.workName.isNullOrEmpty()) {
            updateWorkView.onError("Work name is empty")
        } else {
            MainScope().launch {
                val isSuccessful = workDao.updateWork(work)
                if (isSuccessful) {
                    updateWorkView.onWorkUpdate()
                    workDao.insertUpdateRecord(
                        UpdateRecord(
                            updaterId = updaterId, updateTime = Calendar.getInstance().time,
                            description = "$fullName Update work to ${work.toStringData()}"
                        )
                    )
                    VolleyNetworkRequest.getInstance().sendNotification(
                        work.workId.toString(),
                        "$fullName Update work to ${work.toStringData()}",
                        work.workName!!
                    )
                }
            }
        }
    }

    fun getSourcesAndHandlers(work: Work, roleId: String, userId: Int, fullname: String) {
        MainScope().launch {
            coroutineScope {
                val sources = workDao.getWorks().apply { remove(work) }
                val handlers = when (roleId) {
                    "admin" -> userDao.getUsersOfAdmin()
                    "manager" -> userDao.getUsersOfManager(userId)
                    else -> listOf(User(userId = userId, fullName = fullname))
                }
                updateWorkView.onGetSources(sources)
                updateWorkView.onGetHandlers(handlers)
            }
        }
    }
}