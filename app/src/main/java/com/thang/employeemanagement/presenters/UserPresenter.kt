package com.thang.employeemanagement.presenters

import com.thang.employeemanagement.networking.daos.UserDao
import com.thang.employeemanagement.networking.dtos.User
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class UserPresenter(private val userView: View) {
    private val userDao by lazy { UserDao() }
    fun loadUsers(roleId: String, userId: Int) {
        MainScope().launch {
            val users = if (roleId == "admin") userDao.getUsersOfAdmin() else userDao.getUsersOfManager(userId)
            userView.onUserLoaded(users)
        }
    }

    fun deleteUser(user: User) {
        MainScope().launch {
            val isSuccessful = userDao.deleteUser(user)
            if (isSuccessful) userView.onUserDeleted()
        }
    }

    interface View {
        fun onUserDeleted()
        fun onUserLoaded(users: MutableList<User>)
    }
}