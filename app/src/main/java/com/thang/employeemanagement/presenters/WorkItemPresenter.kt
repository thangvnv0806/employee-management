package com.thang.employeemanagement.presenters

import android.graphics.Bitmap
import android.util.Base64
import com.thang.employeemanagement.adapters.WorkAdapter
import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.UpdateRecord
import com.thang.employeemanagement.networking.dtos.User
import com.thang.employeemanagement.networking.dtos.Work
import com.thang.employeemanagement.networking.volleys.VolleyNetworkRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.util.*

class WorkItemPresenter(val workItemView: View) {
    interface View {
        fun onCommandClick(command: Int, work: Work)
        fun add(menuId: Int, label: String)
        fun onWorkDeleted(position: Int)
        fun onWorkUpdate(position: Int)
    }

    private val workDao by lazy { WorkDao() }

    fun setItemOnClick(roleId: String, userId: Int, work: Work) {
        if (roleId == "admin" || (roleId == "manager" && userId == work.creatorId)) {
            workItemView.onCommandClick(WorkAdapter.UPDATE_COMMAND, work)
        } else {
            workItemView.onCommandClick(WorkAdapter.VIEW_COMMAND, work)
        }
    }

    fun setUpMenu(work: Work, userId: Int, roleId: String) {
        val isHandler = work.handlerId == userId
        val isManagerSelfWork = work.handlerId == userId && work.creatorId == userId && roleId == "manager"
        if (work.creatorId == userId || !isHandler) {
            workItemView.add(Work.DELETED, "Delete Work")
        }
        if (work.status == Work.CREATED && !isHandler) {
            // need confirm from manager or admin
            workItemView.add(Work.CREATED_CONFIRMED, "Accept Work")
            workItemView.add(Work.DECLINE, "Decline Work")
        }
        if (work.status == Work.DECLINE && !isHandler) {
            // after declined, manager can bring it back to Implementing
            workItemView.add(Work.CREATED_CONFIRMED, "Accept Work")
        }
        if (work.status == Work.CREATED_CONFIRMED && !isHandler) {
            // after confirmed, manager can bring it back to decline
            workItemView.add(Work.DECLINE, "Decline Work")
        }
        if (work.status == Work.CREATED_CONFIRMED && (isHandler || isManagerSelfWork)) {
            // after confirmed, handler can start work
            workItemView.add(Work.IMPLEMENTING, "Start work")
        }
        if (work.status == Work.IMPLEMENTING && (isHandler || isManagerSelfWork)) {
            // after start work, handler can mark as Done
            workItemView.add(Work.DONE, "Finish Work")
            workItemView.add(Work.FAILED, "Finish Work")
        }
        if (work.status == Work.DONE && !isHandler) {
            workItemView.add(Work.DONE_CONFIRMED, "Confirm finish work")
            workItemView.add(Work.IMPLEMENTING, "Decline finish work")
        }
        if (work.status == Work.FAILED && !isHandler) {
            workItemView.add(Work.FAILED_CONFIRMED, "Confirm failed work")
            workItemView.add(Work.IMPLEMENTING, "Decline failed work")
        }
    }

    fun deleteWork(work: Work, position: Int, user: User) {
        MainScope().launch {
            val isSuccessful = workDao.deleteWork(work)
            if (isSuccessful) {
                workItemView.onWorkDeleted(position)
                VolleyNetworkRequest.getInstance().sendNotification(
                    work.workId.toString(),
                    "${user.fullName} Update work to ${work.toStringData()}",
                    work.workName!!
                )
            }
        }
    }

    fun updateWork(work: Work, position: Int, user: User) {
        MainScope().launch {
            val isSuccessful = workDao.updateWork(work)
            if (isSuccessful) {
                workItemView.onWorkUpdate(position)
                workDao.insertUpdateRecord(
                    UpdateRecord(
                        updaterId = user.userId!!, updateTime = Calendar.getInstance().time,
                        description = "${user.fullName} Create work ${work.workName}"
                    )
                )
                VolleyNetworkRequest.getInstance().sendNotification(
                    work.workId.toString(),
                    "${user.fullName} Update work to ${work.toStringData()}",
                    work.workName!!
                )
            }
        }
    }


    private suspend fun fromBitmapToBase64(bitmap: Bitmap): String = Dispatchers.Default {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val byteArray: ByteArray = baos.toByteArray()
        Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    fun finishWork(work: Work, position: Int, imgBitmap: Bitmap) {
        MainScope().launch {
            val isSuccessful = workDao.updateWork(work)
            if (isSuccessful) {
                workItemView.onWorkUpdate(position)
                VolleyNetworkRequest.getInstance().also {
                    it.sendNotification(
                        work.workId.toString(),
                        "Update work to ${work.toStringData()}",
                        work.workName!!
                    )
                    it.sendImage(fromBitmapToBase64(imgBitmap), work.verifyingImage!!)
                }
            }
        }
    }
}