package com.thang.employeemanagement.presenters

import com.thang.employeemanagement.networking.daos.WorkDao
import com.thang.employeemanagement.networking.dtos.Work
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class WorkLoadPresenter(private val workLoadView: View) {
    private val workDao: WorkDao by lazy { WorkDao() }
    fun getWorks(roleId: String, userId: Int) {
        MainScope().launch {
            val works = if (roleId == "admin") workDao.getWorks() else workDao.getWorks(userId)
            if (works.isEmpty()) workLoadView.onGetEmptyWork()
            else workLoadView.onGetWorks(works)
        }
    }

    interface View {
        fun onGetEmptyWork()
        fun onGetWorks(works: MutableList<Work>)
    }
}